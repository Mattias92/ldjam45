﻿using LDJam45.Commands;
using LDJam45.Entities;
using LDJam45.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LDJam45
{
    [Serializable()]
    public class EntityManager
    {
        public List<Entity> Entities { get; set; }
        public int IdCounter = 0;
        public Gameboard Gameboard;
        [field: NonSerialized()]
        public event EventHandler<EventArgs> OnLevelCleared;
        [field: NonSerialized()]
        public event EventHandler<EventArgs> OnLevelRestart;

        [field: NonSerialized()]
        private Stack<List<ICommand>> commands;
        [field: NonSerialized()]
        private List<ICommand> commandsThisTurn;
        [field: NonSerialized()]
        private bool isUndoing = false;
        [field: NonSerialized()]
        private bool processingTurn = false;
        [field: NonSerialized()]
        private int lockedFrames = 0;
        [field: NonSerialized()]
        private KeyboardState previousKeyboardState = Keyboard.GetState();
        [field: NonSerialized()]
        private int restartPressedOnce = 0;

        public EntityManager(Gameboard gameboard)
        {
            Gameboard = gameboard;
            Entities = new List<Entity>();
            commands = new Stack<List<ICommand>>();
            commandsThisTurn = new List<ICommand>();
            SortEntities();
        }

        [OnDeserialized]
        public void OnDeserialized(StreamingContext context)
        {
            commands = new Stack<List<ICommand>>();
            commandsThisTurn = new List<ICommand>();
            SortEntities();
        }

        public void SortEntities()
        {
            Entities = Entities.OrderBy(p => p.Y).ThenBy(p => p.X).ToList();
        }

        private void CheckForEquations()
        {
            for (int i = 0; i < Entities.Count; i++)
            {
                Entity entity = Entities[i];
                GetNextNumber(entity.Position.X, entity.Position.Y, "", ref i);
                GetNextOperator(entity.Position.X, entity.Position.Y, "", ref i);
            }
        }

        private string GetNextOperator(float x, float y, string equation, ref int i)
        {
            if (Entities.FirstOrDefault(e => e.Position.X == x && e.Position.Y == y) is Operator op)
            {
                if (op.Symbol == "=")
                {
                    try
                    {
                        DataTable dataTable = new DataTable();
                        string result = dataTable.Compute(equation, "").ToString();

                        if (result != "")
                        {

                            result = Math.Round(Convert.ToDouble(result)).ToString();
                        }

                        if (ContainsNumber(new Vector2(x + 32, y)) && equation != "")
                        {
                            //System.Diagnostics.Debug.WriteLine(equation);

                            if (Regex.IsMatch(result, @"^[0-9]+$"))
                            {
                                if (result.Count() > 3)
                                {
                                    result = "999";
                                }

                                Number number = GetEntity(new Vector2(x + 32, y)) as Number;
                                NumberValueChangedCommand numberValueChangedCommand = new NumberValueChangedCommand(number, number.Value, result);
                                numberValueChangedCommand.Execute();
                                commandsThisTurn.Add(numberValueChangedCommand);
                            }
                            else
                            {
                                Number number = GetEntity(new Vector2(x + 32, y)) as Number;
                                NumberValueChangedCommand numberValueChangedCommand = new NumberValueChangedCommand(number, number.Value, "0");
                                numberValueChangedCommand.Execute();
                                commandsThisTurn.Add(numberValueChangedCommand);
                            }
                        }
                    }
                    catch 
                    {

                    }
                }
                else if (op.Symbol == "==")
                {
                    
                    string leftHandSide = equation;
                    string rightHandSide = GetNextNumber(x + 32, y, "", ref i);

                    if (rightHandSide != "" && leftHandSide != "")
                    {
                        try
                        {
                            DataTable dataTable = new DataTable();
                            string result = dataTable.Compute(leftHandSide + "=" + rightHandSide, "").ToString();

                            if (result == "True")
                            {
                                Sfx.PlaySound("Win2", 0.5f);
                                OnLevelCleared?.Invoke(this, new EventArgs());
                            }
                            else if (result == "False")
                            {
                                
                            }
                        }
                        catch
                        {

                        }
                    }
                }
                else
                {
                    if (equation != "")
                    {
                        equation += op.Symbol;
                    }

                    if (ContainsNumber(new Vector2(x + 32, y)))
                    {
                        i++;
                    }
                    return GetNextNumber(x + 32, y, equation, ref i);
                }
            }
            return equation;
        }

        private string GetNextNumber(float x, float y, string equation, ref int i)
        {
            if (Entities.FirstOrDefault(e => e.Position.X == x && e.Position.Y == y) is Number number)
            {
                if (!Regex.IsMatch(number.Value, @"^[0-9]+$"))
                {
                    return equation;
                }
                equation += number.Value;
                if (ContainsOperator(new Vector2(x + 32, y)))
                {
                    i++;
                }
                return GetNextOperator(x + 32, y, equation, ref i);
            }
            return equation;
        }

        public bool ContainsOperator(Vector2 position)
        {
            return Entities.FirstOrDefault(e => e.Position.X == position.X && e.Position.Y == position.Y) is Operator;
        }

        public bool ContainsNumber(Vector2 position)
        {
            return Entities.FirstOrDefault(e => e.Position.X == position.X && e.Position.Y == position.Y) is Number;
        }

        public bool ContainsEntity(Vector2 position)
        {
            return Entities.FirstOrDefault(e => e.Position.X == position.X && e.Position.Y == position.Y) != null;
        }

        public Entity GetEntity(Vector2 position)
        {
            return Entities.FirstOrDefault(e => e.Position.X == position.X && e.Position.Y == position.Y);
        }

        public bool CanPushEntity(Vector2 position, Vector2 next)
        {
            Entity entity = GetEntity(position);

            if (!entity.CanMove)
            {
                return false;
            }

            if (Gameboard.IsWall(position + next))
            {
                return false;
            }
            else
            {
                if (ContainsEntity(position + next))
                {
                    return CanPushEntity(position + next, next);
                }
                else
                {
                    return true;
                }
            }
        }

        public void PushEntities(Vector2 position, Vector2 next)
        {
            Entity entity = GetEntity(position);
            MoveCommand moveCommand = new MoveCommand(entity, position, position + next);
            moveCommand.Execute();
            commandsThisTurn.Add(moveCommand);

            if (ContainsEntity(position + next))
            {
                PushEntities(position + next, next);
            }
        }

        public bool PlayerMove(Vector2 next)
        {
            bool performedActions = false;

            for (int i = 0; i < Entities.Count; i++)
            {
                if (Entities[i] is Player player)
                {
                    if (Gameboard.IsWall(player.Position + next))
                    {
                        continue;
                    }

                    if (ContainsEntity(player.Position + next))
                    {
                        bool canPush = false;
                        canPush = CanPushEntity(player.Position + next, next);
                        if (canPush)
                        {
                            PushEntities(player.Position + next, next);
                            MoveCommand moveCommand = new MoveCommand(player, player.Position, player.Position + next);
                            moveCommand.Execute();
                            commandsThisTurn.Add(moveCommand);
                            performedActions = true;
                        }
                    }
                    else
                    {
                        MoveCommand moveCommand = new MoveCommand(player, player.Position, player.Position + next);
                        moveCommand.Execute();
                        commandsThisTurn.Add(moveCommand);
                        performedActions = true;
                    }
                }
            }

            return performedActions;
        }

        public void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            KeyboardState keyboardState = Keyboard.GetState();
            bool performedActions = false;

            if (processingTurn)
            {
                lockedFrames--;
                if (lockedFrames == 0)
                {
                    SortEntities();
                    CheckForEquations();

                    if (isUndoing)
                    {
                        commandsThisTurn.Clear();
                        isUndoing = false;
                    }
                    else
                    {
                        commands.Push(commandsThisTurn);
                        commandsThisTurn = new List<ICommand>();
                    }

                    SortEntities();
                    processingTurn = false;
                }
            }

            if (restartPressedOnce > 0)
            {
                restartPressedOnce--;
            }

            if (!processingTurn)
            {
                if (keyboardState.IsKeyDown(Keys.W))
                {
                    performedActions = PlayerMove(new Vector2(0f, -32f));
                }
                else if (keyboardState.IsKeyDown(Keys.D))
                {
                    performedActions = PlayerMove(new Vector2(32f, 0f));
                }
                else if (keyboardState.IsKeyDown(Keys.S))
                {
                    performedActions = PlayerMove(new Vector2(0f, 32f));
                }
                else if (keyboardState.IsKeyDown(Keys.A))
                {
                    performedActions = PlayerMove(new Vector2(-32f, 0f));
                }
                else if (keyboardState.IsKeyDown(Keys.Z))
                {
                    if (commands.Count > 0)
                    {
                        List<ICommand> commandsToUndo = commands.Pop();

                        for (int i = 0; i < commandsToUndo.Count; i++)
                        {
                            commandsToUndo[i].Undo();
                        }

                        isUndoing = true;
                        performedActions = true;
                    }
                }
                else if (keyboardState.IsKeyDown(Keys.R) && previousKeyboardState.IsKeyUp(Keys.R))
                {
                    if (restartPressedOnce > 0)
                    {
                        OnLevelRestart?.Invoke(this, new EventArgs());
                    }
                    restartPressedOnce = 30;
                }

                previousKeyboardState = keyboardState;
            }

            if (performedActions)
            {
                Sfx.PlaySound("Walk", 0.05f);
                processingTurn = true;
                lockedFrames = 10;
            }

            for (int i = 0; i < Entities.Count; i++)
            {
                Entity entity = Entities[i];
                if (entity.IsMoving)
                {
                    // Moving upwards
                    if (entity.Next.Y < entity.Y)
                    {
                        entity.Y += -3.5f;
                        if (entity.Next.Y >= entity.Y)
                        {
                            entity.Y = entity.Next.Y;
                            entity.IsMoving = false;
                        }
                    }
                    // Moving right
                    else if (entity.Next.X > entity.X)
                    {
                        entity.X += 3.5f;
                        if (entity.Next.X <= entity.X)
                        {
                            entity.X = entity.Next.X;
                            entity.IsMoving = false;
                        }
                    }
                    // Moving downwards
                    else if (entity.Next.Y > entity.Y)
                    {
                        entity.Y += 3.5f;
                        if (entity.Next.Y <= entity.Y)
                        {
                            entity.Y = entity.Next.Y;
                            entity.IsMoving = false;
                        }
                    }
                    // Moving left
                    else if (entity.Next.X < entity.X)
                    {
                        entity.X += -3.5f;
                        if (entity.Next.X >= entity.X)
                        {
                            entity.X = entity.Next.X;
                            entity.IsMoving = false;
                        }
                    }
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont, Texture2D atlas)
        {
            for (int i = 0; i < Entities.Count; i++)
            {
                Entity entity = Entities[i];
                entity.Draw(spriteBatch, spriteFont, atlas);
            }
        }
    }
}
