﻿using LDJam45.Entities;
using LDJam45.Utility;
using LDJam45.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Data;
using System.Linq.Expressions;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System;
using Microsoft.Xna.Framework.Audio;

namespace LDJam45
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //private Gameboard gameboard;
        private EntityManager entityManager;
        private Texture2D atlas;
        private SpriteFont spriteFont;
        private Editor editor;
        private string currentLevel = "";
  
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.Title = "Null Is Zero";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //gameboard = new Gameboard();
            //entityManager = new EntityManager(gameboard);

            base.Initialize();

            //DataTable test = new DataTable();
            //var result = test.Compute("4*4", "");
            //System.Diagnostics.Debug.WriteLine(result.ToString());
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>d
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            atlas = Content.Load<Texture2D>("atlas8");
            spriteFont = Content.Load<SpriteFont>("font");

            Sfx.SoundEffects.Add("Walk", Content.Load<SoundEffect>("Walk"));
            Sfx.SoundEffects.Add("Win2", Content.Load<SoundEffect>("Win2"));
            currentLevel = "LevelOne";
            Load(currentLevel);
        }

        private void Load(string level)
        {
            string dir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Levels/" + level + ".bin");

            if (File.Exists(dir))
            {
                Stream openFileStream = File.OpenRead(dir);
                BinaryFormatter deserializer = new BinaryFormatter();
                entityManager = (EntityManager)deserializer.Deserialize(openFileStream);
                openFileStream.Close();
            }

            entityManager.OnLevelCleared += OnLevelCleared;
            entityManager.OnLevelRestart += OnLevelRestart;
            //editor = new Editor(GraphicsDevice.Viewport, entityManager, entityManager.Gameboard);
        }

        private void OnLevelCleared(object sender, EventArgs args)
        {
            if (currentLevel == "LevelOne")
            {
                currentLevel = "LevelTwo";
                Load(currentLevel);
            }
            else if (currentLevel == "LevelTwo")
            {
                currentLevel = "LevelThree";
                Load(currentLevel);
            }
            else if (currentLevel == "LevelThree")
            {
                currentLevel = "LevelFour";
                Load(currentLevel);
            }
            else if (currentLevel == "LevelFour")
            {
                currentLevel = "LevelFive";
                Load(currentLevel);
            }
            else if (currentLevel == "LevelFive")
            {
                currentLevel = "Credits";
                Load(currentLevel);
            }
            else if (currentLevel == "Credits")
            {
                Exit();
            }
        }

        private void OnLevelRestart(object sender, EventArgs args)
        {
            Load(currentLevel);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            //{
            //    Exit();
            //}

            entityManager.Update(gameTime);
            //editor.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            //GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            entityManager.Gameboard.Draw(spriteBatch, atlas);
            entityManager.Draw(spriteBatch, spriteFont, atlas);
            //editor.Draw(spriteBatch, atlas);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
