﻿using LDJam45.Entities;
using LDJam45.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace LDJam45.Utility
{
    public class Editor
    {
        private EntityManager entityManager;
        private Gameboard gameboard;

        private List<EditorOption> options;
        private EditorOption selectedOption;
        private int width;
        private int height;
        private bool readInput = false;
        private string currentInput = "0";

        private MouseState previousMouseState = Mouse.GetState();
        private KeyboardState previousKeyboardState = Keyboard.GetState();

        public Editor(Viewport viewport, EntityManager entityManager, Gameboard gameboard)
        {
            width = viewport.Width;
            height = viewport.Height;

            this.entityManager = entityManager;
            this.gameboard = gameboard;
            options = new List<EditorOption>()
            {
                new EditorOption(0, height, 0, 5, "RemoveEntity"),
                new EditorOption(1, height, 0, 4, "Floor"),
                new EditorOption(2, height, 0, 0, "Wall-North"),
                new EditorOption(3, height, 1, 0, "Wall-Sides"),
                new EditorOption(4, height, 2, 0, "Wall-East"),
                new EditorOption(5, height, 3, 0, "Wall-West"),
                new EditorOption(6, height, 0, 1, "Wall-South"),
                new EditorOption(7, height, 1, 1, "Wall-NorthEast"),
                new EditorOption(8, height, 2, 1, "Wall-NorthWest"),
                new EditorOption(9, height, 0, 2, "Player"),
                new EditorOption(10, height, 1, 2, "Number"),
                new EditorOption(11, height, 2, 2, "Equality"),
                new EditorOption(12, height, 3, 2, "Equals"),
                new EditorOption(13, height, 0, 3, "Minus"),
                new EditorOption(14, height, 1, 3, "Plus"),
                new EditorOption(15, height, 2, 3, "Multiplication"),
                new EditorOption(16, height, 3, 3, "Division"),
                new EditorOption(17, height, 1, 4, "Wall-SidesEnd"),
                new EditorOption(18, height, 2, 4, "Wall-SouthWest"),
                new EditorOption(19, height, 3, 4, "Wall-SouthEast"),
                new EditorOption(20, height, 3, 1, "Wall-WestEast"),
            };
        }

        public bool SelectOption(int x, int y)
        {
            EditorOption editorOption = options.FirstOrDefault(o => x >= o.X && x < o.X + 32 && y >= o.Y && y < o.Y + 32);
            if (editorOption != null)
            {
                selectedOption = editorOption;
                return true;
            }
            return false;
        }

        public void UseOption(int x, int y)
        {
            x = (x / 32) * 32;
            y = (y / 32) * 32;

            if (selectedOption.Name == "Floor")
            {
                gameboard.SetTile(x, y, false, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-North")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-Sides")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-East")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-West")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-South")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-NorthEast")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-NorthWest")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "RemoveEntity")
            {
                entityManager.Entities.RemoveAll(e => e.X == x && e.Y == y);
            }
            else if (selectedOption.Name == "Player")
            {
                entityManager.Entities.Add(new Player(entityManager.IdCounter++, new Vector2(x, y), true, currentInput));
            }
            else if (selectedOption.Name == "Number")
            {
                entityManager.Entities.Add(new Number(entityManager.IdCounter++, new Vector2(x, y), true, currentInput));
            }
            else if (selectedOption.Name == "Equality")
            {
                entityManager.Entities.Add(new Equality(entityManager.IdCounter++, new Vector2(x, y), true));
            }
            else if (selectedOption.Name == "Equals")
            {
                entityManager.Entities.Add(new Equals(entityManager.IdCounter++, new Vector2(x, y), true));
            }
            else if (selectedOption.Name == "Minus")
            {
                entityManager.Entities.Add(new Minus(entityManager.IdCounter++, new Vector2(x, y), true));
            }
            else if (selectedOption.Name == "Plus")
            {
                entityManager.Entities.Add(new Plus(entityManager.IdCounter++, new Vector2(x, y), true));
            }
            else if (selectedOption.Name == "Multiplication")
            {
                entityManager.Entities.Add(new Multiplication(entityManager.IdCounter++, new Vector2(x, y), true));
            }
            else if (selectedOption.Name == "Division")
            {
                entityManager.Entities.Add(new Division(entityManager.IdCounter++, new Vector2(x, y), true));
            }
            else if (selectedOption.Name == "Wall-SidesEnd")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-SouthWest")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-SouthEast")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }
            else if (selectedOption.Name == "Wall-WestEast")
            {
                gameboard.SetTile(x, y, true, selectedOption.U, selectedOption.V);
            }

            entityManager.SortEntities();
        }

        public void Save()
        {
            string dir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Levels/Credits.bin");

            Stream SaveFileStream = File.Create(dir);
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(SaveFileStream, entityManager);
            SaveFileStream.Close();
        }

        public void Load()
        {
            string dir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Levels/LevelOne.bin");

            if (File.Exists(dir))
            {
                Stream openFileStream = File.OpenRead(dir);
                BinaryFormatter deserializer = new BinaryFormatter();
                entityManager = (EntityManager)deserializer.Deserialize(openFileStream);
                openFileStream.Close();
            }
        }

        public void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            KeyboardState keyboardState = Keyboard.GetState();

            int x = mouseState.X;
            int y = mouseState.Y;

            if (mouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released)
            {
                if (!SelectOption(x, y))
                {
                    if (selectedOption != null)
                    {
                        UseOption(x, y);
                    }
                }
            }

            if (keyboardState.IsKeyDown(Keys.F1) && previousKeyboardState.IsKeyUp(Keys.F1))
            {
                Save();
            }
            else if (keyboardState.IsKeyDown(Keys.F2) && previousKeyboardState.IsKeyUp(Keys.F2))
            {
                Load();
            }

            if (readInput)
            {
                if (keyboardState.IsKeyDown(Keys.D0) && previousKeyboardState.IsKeyUp(Keys.D0))
                {
                    currentInput += "0";
                }
                else if (keyboardState.IsKeyDown(Keys.D1) && previousKeyboardState.IsKeyUp(Keys.D1))
                {
                    currentInput += "1";
                }
                else if (keyboardState.IsKeyDown(Keys.D2) && previousKeyboardState.IsKeyUp(Keys.D2))
                {
                    currentInput += "2";
                }
                else if (keyboardState.IsKeyDown(Keys.D3) && previousKeyboardState.IsKeyUp(Keys.D3))
                {
                    currentInput += "3";
                }
                else if (keyboardState.IsKeyDown(Keys.D4) && previousKeyboardState.IsKeyUp(Keys.D4))
                {
                    currentInput += "4";
                }
                else if (keyboardState.IsKeyDown(Keys.D5) && previousKeyboardState.IsKeyUp(Keys.D5))
                {
                    currentInput += "5";
                }
                else if (keyboardState.IsKeyDown(Keys.D6) && previousKeyboardState.IsKeyUp(Keys.D6))
                {
                    currentInput += "6";
                }
                else if (keyboardState.IsKeyDown(Keys.D7) && previousKeyboardState.IsKeyUp(Keys.D7))
                {
                    currentInput += "7";
                }
                else if (keyboardState.IsKeyDown(Keys.D8) && previousKeyboardState.IsKeyUp(Keys.D8))
                {
                    currentInput += "8";
                }
                else if (keyboardState.IsKeyDown(Keys.D9) && previousKeyboardState.IsKeyUp(Keys.D9))
                {
                    currentInput += "9";
                }
                else if (keyboardState.IsKeyDown(Keys.L) && previousKeyboardState.IsKeyUp(Keys.L))
                {
                    currentInput += "L";
                }
                else if (keyboardState.IsKeyDown(Keys.E) && previousKeyboardState.IsKeyUp(Keys.E))
                {
                    currentInput += "E";
                }
                else if (keyboardState.IsKeyDown(Keys.V) && previousKeyboardState.IsKeyUp(Keys.V))
                {
                    currentInput += "V";
                }
                else if (keyboardState.GetPressedKeys().Count() > 0 && previousKeyboardState.GetPressedKeys().Count() == 0)
                {
                    currentInput += keyboardState.GetPressedKeys().First();
                }
            }

            if (keyboardState.IsKeyDown(Keys.Q) && previousKeyboardState.IsKeyUp(Keys.Q))
            {
                if (readInput)
                {
                    readInput = false;
                    System.Diagnostics.Debug.WriteLine("Stopped Reading Input");
                }
                else
                {
                    readInput = true;
                    currentInput = "";
                    System.Diagnostics.Debug.WriteLine("Started Reading Input");
                }

            }

            previousMouseState = mouseState;
            previousKeyboardState = keyboardState;
        }

        public void Draw(SpriteBatch spriteBatch, Texture2D atlas)
        {
            for (int i = 0; i < options.Count; i++)
            {
                EditorOption eo = options[i];
                spriteBatch.Draw(atlas, new Vector2(eo.X, eo.Y), new Rectangle(eo.U * 32, eo.V * 32, 32, 32), Color.White);
            }
        }
    }
}
