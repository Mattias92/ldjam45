﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam45.Utility
{
    public class EditorOption
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int U { get; set; }
        public int V { get; set; }
        public string Name { get; set; }

        public EditorOption(int x, int y, int u, int v, string name)
        {
            X = x * 32;
            Y = y - 32;
            U = u;
            V = v;
            Name = name;
        }
    }
}
