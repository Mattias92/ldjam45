﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam45.World
{
    [Serializable()]
    public class Tile
    {
        public bool IsSolid;
        public int U;
        public int V;

        public Tile(bool isSolid, int u, int v)
        {
            IsSolid = isSolid;
            U = u;
            V = v;
        }
    }
}
