﻿using LDJam45.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LDJam45.World
{
    [Serializable()]
    public class Gameboard
    {
        public int Width = 25;
        public int Height = 16;
        private Tile[,] tiles;

        public Gameboard()
        {
            tiles = new Tile[Width, Height];
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    tiles[x, y] = new Tile(false, 0, 4);
                }
            }
        }

        [OnDeserialized]
        public void OnDeserialized(StreamingContext context)
        {
            
        }

        [OnSerialized()]
        public void OnSerialized(StreamingContext context)
        {
            
        }

        public void SetTile(int x, int y, bool isWall, int u, int v)
        {
            x = x / 32;
            y = y / 32;
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                tiles[x, y] = new Tile(isWall, u, v);
            }
        }

        public bool IsWall(Vector2 position)
        {
            int x = (int)position.X / 32;
            int y = (int)position.Y / 32;

            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                return tiles[x, y].IsSolid;
            }
            return false;
        }

        public void Draw(SpriteBatch spriteBatch, Texture2D atlas)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Tile tile = tiles[x, y];
                    if (tile != null)
                    {
                        spriteBatch.Draw(atlas, new Rectangle(x * 32, y * 32, 32, 32), new Rectangle(tile.U * 32, tile.V * 32, 32, 32), Color.White);
                    }
                }
            }
        }
    }
}
