﻿using LDJam45.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam45.Commands
{
    public class MoveCommand : ICommand
    {
        private Entity entity;
        private Vector2 prev;
        private Vector2 next;

        public MoveCommand(Entity entity, Vector2 prev, Vector2 next)
        {
            this.entity = entity;
            this.prev = prev;
            this.next = next;
        }

        public void Execute()
        {
            entity.Next = next;
            entity.IsMoving = true;
        }

        public void Undo()
        {
            entity.Next = prev;
            entity.IsMoving = true;
        }
    }
}
