﻿using LDJam45.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam45.Commands
{
    public class NumberValueChangedCommand : ICommand
    {
        private Number number;
        private string oldValue;
        private string newValue;

        public NumberValueChangedCommand(Number number, string oldValue, string newValue)
        {
            this.number = number;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        public void Execute()
        {
            number.Value = newValue;
        }

        public void Undo()
        {
            number.Value = oldValue;
        }
    }
}
