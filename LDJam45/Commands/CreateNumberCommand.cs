﻿using LDJam45.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam45.Commands
{
    public class CreateNumberCommand : ICommand
    {
        private List<Entity> entities;
        private int id;
        private Vector2 position;
        private string value;

        public CreateNumberCommand(List<Entity> entities, int id, Vector2 position, string value)
        {
            this.entities = entities;
            this.id = id;
            this.position = position;
            this.value = value;
        }

        public void Execute()
        {
            entities.Add(new Number(id, position, true, value));
        }

        public void Undo()
        {
            entities.RemoveAll(e => e.Id == id);
        }
    }
}
