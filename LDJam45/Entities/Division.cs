﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LDJam45.Entities
{
    [Serializable()]
    public class Division : Operator
    {
        public Division(int id, Vector2 position, bool canMove, string symbol = "/") : base(id, position, canMove, symbol)
        {
            U = 3;
            V = 3;  
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont, Texture2D atlas)
        {
            spriteBatch.Draw(atlas, Position, new Rectangle(U * 32, V * 32, 32, 32), Color.White);
        }
    }
}
