﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam45.Entities
{
    [Serializable()]
    public abstract class Entity 
    {
        public int Id { get; set; }

        private float _x;
        public float X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
            }
        }

        private float _y;
        public float Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
            }
        }

        public Vector2 Position
        {
            get
            {
                return new Vector2(_x, _y);
            }
            set
            {
                _x = value.X;
                _y = value.Y;
            }
        }

        [field: NonSerialized()]
        public Vector2 Next;
        public bool CanMove { get; set; }
        public bool IsMoving { get; set; }

        protected int U;
        protected int V;

        public Entity(int id, Vector2 position, bool canMove) 
        {
            Id = id;
            U = 0;
            V = 0;
            Position = position;
            CanMove = canMove;
            IsMoving = false;
        }

        public virtual void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont, Texture2D atlas)
        {

        }
    }
}
