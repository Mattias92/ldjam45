﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LDJam45.Entities
{
    [Serializable()]
    public class Operator : Entity
    {
        public string Symbol { get; set; }

        public Operator(int id, Vector2 position, bool canMove, string symbol) : base(id, position, canMove)
        {
            Symbol = symbol;
            U = 2;
            V = 2;
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont, Texture2D atlas)
        {
            spriteBatch.Draw(atlas, Position, new Rectangle(U * 32, V * 32, 32, 32), Color.White);
            spriteBatch.DrawString(spriteFont, Symbol, Position, Color.Black);
        }
    }
}
