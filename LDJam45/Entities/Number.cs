﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LDJam45.Entities
{
    [Serializable()]
    public class Number : Entity
    {
        public string Value { get; set; }

        public Number(int id, Vector2 position, bool canMove, string value) : base(id, position, canMove)
        {
            Value = value;
            U = 1;
            V = 2;
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont, Texture2D atlas)
        {
            Vector2 fontSize = spriteFont.MeasureString(Value);
            Vector2 textCenter = new Vector2(fontSize.X / 2, fontSize.Y / 2);
            spriteBatch.Draw(atlas, Position, new Rectangle(U * 32, V * 32, 32, 32), Color.White);
            spriteBatch.DrawString(spriteFont, Value, new Vector2(Position.X + 16f - textCenter.X, Position.Y + 16f - textCenter.Y), Color.Black);
        }
    }
}
